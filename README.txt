1. Thầy/cô chạy phần backend trên Visual Studio với port localhost:44308 ạ

2. Về tài khoản trong người dùng trong website, thầy/cô có thể đăng kí
tài khoản và đăng nhập ạ. Nếu không thầy/cô có thể sử dụng tài khoản sau:
 + Tên tài khoản: thanhtuan67@gmail.com
 + Mật khẩu: 123456

3. Thầy/cô sử dụng tài khoản PayPal sau để thực hiện chức năng thanh toán 
bằng PayPal ạ:
 + Tên tài khoản: demo@ou.edu.vn
 + Mật khẩu: 12345678

4. Để kiểm tra thông tin giao dịch, thầy/cô truy cập vào trang 
sandbox.paypal.com và đăng nhập bằng tài khoản đã được cấp trên ạ