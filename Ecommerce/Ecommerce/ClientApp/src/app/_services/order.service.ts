import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class OrderService {
    constructor(private http: HttpClient) { }
    CreateOrder(Order) {
        return this.http.post(`${environment.SERVER_URL}/Order/Create-Order`, Order);
    }

    CreateOrderDetail(OrderDetail) {
        return this.http.post(`${environment.SERVER_URL}/Ordetail/Create-OrderDetail`, OrderDetail);
    }

}