import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';


declare var $;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  host: {
    '(window:resize)': 'onWindowResize($event)'
  }
})
export class MenuComponent implements OnInit {
  stock = 0;
  productId: number;
  public randomProduct: any = [];
  contactFlag = true;
  public listCategoryName: any = {
    data: []
  };

  checkHover = true;
  catePhoneName: any = {
    data: []
  };
  cateElectronicName = {};
  isMobile = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth = 1366;

  // Mảng chứa sp giao diện reponsive
  productsPhone: any = {
    data: [],
  };
  productsElectronic: any = {
    data: [],
  };

  contact: any = {
    fullName: '',
    email: '',
    phoneNumber: '',
    message: '',
  };

  categories: any = [
    {
      name: 'Điện thoại máy tính bảng',
      image: 'https://salt.tikicdn.com/ts/category/93/27/e3/192b0ebe1d4658c51f9931bda62489b2.png'
    },
    {
      name: 'Điện thoại Smartphone',
      image: 'https://salt.tikicdn.com/cache/280x280/ts/product/5a/7b/e1/5acd19c60380413b3e72ac3460da0f62.jpg'
    },
    {
      name: 'Máy tính bảng',
      image: 'https://salt.tikicdn.com/cache/280x280/ts/product/d9/7a/32/0996fcf1395e3120d6dba80583ff6cd9.jpg'
    },
    {
      name: 'Thực phẩm tươi sống',
      image: 'https://salt.tikicdn.com/ts/category/a6/9f/45/460fdecbbe0f81da09c7da37aa08f680.png'
    },
    {
      name: 'Đồ Chơi - Mẹ & Bé',
      image: 'https://salt.tikicdn.com/ts/category/66/15/4f/6282e8c6655cb87cb226e3b701bb9137.png'
    },
    {
      name: 'Làm Đẹp - Sức Khỏe',
      image: 'https://salt.tikicdn.com/ts/category/85/13/02/d8e5cd75fd88862d0f5f647e054b2205.png'
    },
    {
      name: 'Điện thoại máy tính bảng',
      image: 'https://salt.tikicdn.com/ts/category/93/27/e3/192b0ebe1d4658c51f9931bda62489b2.png'
    },
    {
      name: 'Điện thoại máy tính bảng',
      image: 'https://salt.tikicdn.com/ts/category/93/27/e3/192b0ebe1d4658c51f9931bda62489b2.png'
    },
  ];

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
  }

  ngOnInit() {
    this.isMobile = this.width < this.mobileWidth;
    setTimeout(this.slider, 100);
    setTimeout(this.sliderCategory, 1);


    this.getProductPhone(1);
    this.getProductElectronic(2);

    setTimeout(() => {
      this.loadProductRandom();
    }, 1000);

  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
  }

  random(min, max) {
    // tslint:disable-next-line:radix
    return parseInt(Math.random() * (max - min) + min);
  }

  // Chọn loại sản phẩm Điện thoại - Máy tính bảng trang chủ responsive
  onSelectedProId(id) {
    var x = {
      productId: id,
      page: 1,
      size: 48,
    }
    this.http.post(`${environment.SERVER_URL}/Product/get-product-detail-by-product-id`, x)
      .subscribe(result => {
        let pro: any = {
          data: []
        };
        pro = result;
        pro = pro.data;
        this.productId = pro.data[0].productId;
        this.productsPhone = pro;
      }, error => console.error(error));
  }

  // Chọn loại sản phẩm Điện tử - Điện lạnh trang chủ responsive
  onSelectedProElectronicId(id) {
    var x = {
      productId: id,
      page: 1,
      size: 48,
    }
    this.http.post(`${environment.SERVER_URL}/Product/get-product-detail-by-product-id`, x)
      .subscribe(result => {
        let pro: any = {
          data: []
        };
        pro = result;
        pro = pro.data;
        this.productId = pro.data[0].productId;
        this.productsElectronic = pro;
      }, error => console.error(error));
  }

  // Lấy random sản phẩm
  getProductRandom(id) {
    this.http.post(`${environment.SERVER_URL}/ProductDetail/get-by-id`, { id }).subscribe((result) => {

      let getProduct: any = {};
      getProduct = result;
      getProduct = getProduct.data;
      this.randomProduct.push(getProduct);
    });
  }

  // Load sản phẩm random
  loadProductRandom() {
    let interval;
    interval = setInterval(() => {
      if (this.randomProduct.length === 10) {
        clearInterval(interval);
      } else {
        this.getProductRandom(this.random(1367, 1573));
        this.getProductRandom(this.random(1, 431));
      }
    }, 250)
  }

  // Load sản phẩm điện thoại - máy tính bảng
  getProductPhone(cPage) {
    let x = {
      page: cPage,
      size: 48,
      keyword: "",
      categoryId: 1,
    };
    this.http
      .post(
        `${environment.SERVER_URL}/ProductDetail/get-product-by-categoryName-linq`,
        x
      )
      .subscribe(
        (result) => {
          this.productsPhone = result;
          this.productsPhone = this.productsPhone.data;
          this.getProductPhoneName(this.productsPhone.data[0].categoryId);
        },
        (error) => console.error(error)
      );
  }

  // Load sản phẩm điện thoại - máy tính bảng
  getProductElectronic(cPage) {
    let x = {
      page: cPage,
      size: 48,
      keyword: "",
      categoryId: 2,
    };
    this.http
      .post(
        `${environment.SERVER_URL}/ProductDetail/get-product-by-categoryName-linq`,
        x
      )
      .subscribe(
        (result) => {
          this.productsElectronic = result;
          this.productsElectronic = this.productsElectronic.data;
          this.getProductElectronicName(this.productsElectronic.data[0].categoryId);
        },
        (error) => console.error(error)
      );
  }

  // Lấy tên loại sản phẩm để hiển thị trên danh mục sản phẩm
  getProductPhoneName(id) {
    let x = {
      categoryId: id
    }
    this.http.post(`${environment.SERVER_URL}/Product/get-cateproduct-by-id`, x)
      .subscribe(result => {
        this.catePhoneName = result;
      }, error => console.error(error));
  }

  // Lấy tên loại sản phẩm để hiển thị trên danh mục sản phẩm
  getProductElectronicName(id) {
    const x = {
      categoryId: id
    };
    this.http.post(`${environment.SERVER_URL}/Product/get-cateproduct-by-id`, x)
      .subscribe(result => {
        this.cateElectronicName = result;
      }, error => console.error(error));
  }

  // Thêm thông tin liên lạc
  addContact() {
    const x = this.contact;

    if (x.fullName === '' || x.email === '' || x.phoneNumber === '' || x.message === '') {
      this.contactFlag = false;
    } else {
      this.contactFlag = true;
      this.http.post(`${environment.SERVER_URL}/Contact/create-contact`, x).subscribe(result => {
      const res: any = result;
        if (res.success) {
          this.contact = res.data;
          alert('Thêm mới thành công!');
          location.reload();
        }
      }, error => console.error(error));
    }
  }

  slider() {
    $('.menu-banner-slick').slick({
      speed: 600,
      autoplay: true,
      autoplaySpeed: 10000,
      infinite: true,
      slidesToScroll: 1,
      prevArrow: '.slick-btn-prev',
      nextArrow: '.slick-btn-next'
    });
  }

  sliderCategory() {
    $('.slider-list').slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 3,
      prevArrow: '.prev',
      nextArrow: '.next'
    });
  }



  // check2 = !this.check1;
}
