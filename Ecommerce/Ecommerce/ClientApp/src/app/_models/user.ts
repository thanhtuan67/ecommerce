export class User {
    userId: number;
    email: string;
    password: string;
    fullName: string;
    phonenumber: string;
    address : string;
    roleId: number;
    token?: string;
}